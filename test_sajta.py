from app import cene_api, logovanje_bez_parametara,unos_knjige,edit_knjige,obrisi_knjigu,ulogovani_korisnik,vrati_naslov_api,ekstenzija_api,id_broj
from lokal import server_naslov_cena,podaci_lokal

def test_sa_sajta_proizvodi():
    assert podaci_lokal() == server_naslov_cena()
def test_ulogovanog_korisnika():
    assert "Welcome : irina" in ulogovani_korisnik()
def test_logovanja_bez_parametara():
    assert not "Welcome :" in logovanje_bez_parametara()
def test_unos():
    assert "Uspesno ste dodali novu knjigu!" in unos_knjige()
def test_izmena():
    assert "Uspesno ste izmenili podatke!" in edit_knjige()
def test_brsisi():
    assert "Uspesno ste izbrisali knjigu!" in obrisi_knjigu()
def test_naslova():
    for i in vrati_naslov_api():
        assert "" in i
def test_ekstenzije():
    for i in ekstenzija_api():
        assert ".jpg" in i
def test_id():
    for i in id_broj():
        assert i.isnumeric()
def test_cene():
    for i in cene_api():
        provera = float(i)
        assert isinstance(provera, float)